# Laboratorio de Microprocesadores, segundo cuatrimestre 2023

## Trabajo Práctico 2:

El objetivo de este trabajo práctico es investigar los temporizadores (_timers_) como periféricos capaces de medir tiempo y generar señales de modulación por ancho de pulso (_PWM: "Pulse Width Modulation"_).

## Actividades


### Timer
1. Desarrollar un _firmware_ que por cada vez que se presione un pulsador encienda un _LED_ por exactamente 2 segundos, usando un _timer_. Implementar el encendido del _LED_ por interrupción externa y el apagado del _LED_ por interrupción de _timer_, al ocurrir el _overflow_ correspondiente. Ensamblarlo, grabarlo en el microcontrolador y verificar su funcionamiento.
2. Comparar el valor de configuración de los registros del _timer_ con los demás grupos de trabajo. ¿Hay variaciones?

### PWM
3. Desarrollar un _firmware_ que maneje el brillo de un _LED_ conectado a una salida del microcontrolador capaz de manejar _PWM_. El brillo se debe aumentar o disminuir en saltos de 20% de _duty cycle_ al presionar un pulsador para aumentarlo y otro para disminuirlo. Ensamblarlo, grabarlo en el microcontrolador y verificar su funcionamiento.
4. Obtener de Internet el circuito esquemático del módulo controlador de motores y las hojas de datos de los integrados usados. Analizar el circuito, en particular la porción capaz de manejar motores de corriente continua usando _PWM_. 
5. Investigar y analizar el arreglo circuital "puente H", usado para regular velocidad y sentido de motores de continua.
6. Desarrollar un _firmware_ que varíe la velocidad de un motor de continua conectado a una rueda, usando dos pulsadores, uno para subir la velocidad y otro para bajar la velocidad. Levantar una tabla de los registros de configuración del periférico _PWM_ para 30, 40, 50 y 60 revoluciones por minuto (_RPM_).  (*)
7. Si se hace girar el motor en sentido contrario, modificando el sentido mediante el _firmware_, hay cambios en los valores levantados?

(*) Sugerencia: para medir la cantidad de vueltas por minuto ("_RPM_"), pegar un pedazo de papel blanco a la rueda negra y enfrentar un sensor de línea a la rueda. Elevar el móvil para que la ruede gire en vacío. Cuando el sensor de línea pase frente al trozo de papel blanco, se observará un pulso a la salida del sensor de línea, el cual puede medirse con osciloscopio.

## Materiales

* 1 controlador de motor de continua
* 1 sensor de línea
* móvil con ruedas de auto seguidor de línea
* 2 pulsadores
* 1 microcontrolador ATmega328p
* Proboboard
* Cables Dupont
* Programador AVR (Club de Robótica/Arduino/etcétera).
