# Laboratorio de Microprocesadores, primer cuatrimestre 2023

## Requerimientos generales de los Trabajos Prácticos

### Carátula

Incluir nombre completo del estudiante y padrón, docentes a cargo del turno, título del TP y fecha de entrega.

### Objetivos

Explicitar cuáles son los objetivos, es decir a qué se quiere llegar. (*¿Qué se hará?*)

### Descripción

En esta sección punto se detallarán los procedimientos que se realizarán para concretar los objetivos mencionados anteriormente. (*¿Cómo se hará?*). El alcance de esta sección depende en gran medida de las consignas particulares del enunciado del Trabajo Práctico en cuestión. Se sugiere leer atentamente los requerimientos.

### Hardware 

Para documentar el hardware, se debe incluir:

* Diagrama de bloques: debe describir los componentes de hardware a nivel macro (sin detalles).

* Circuito esquemático: constituye la especificaciòn detallada del hardware empleado. El esquemático debe representar fielmente *TODO* el circuito utilizado a nivel de componentes, no a nivel de bloques funcionales. 

### Firmware / Software

Para documentar la implementación realizada, se debe incluir:

* Diagrama de flujo: esta representación permitirá entender rápidamente la lógica de la aplicación, omitiendo los detalles de implementación, ya que es un esquema macro.
* Código fuente del programa: el listado de programa consituye la documentación detallada de la implementación. los fuentes se deberán comentar adecuadamente. En particular, incluir prototipos en cada subrutina.
* Si hay programas accesorios, incluir links de descarga o fuentes. Por ejemplo, un terminal de comunicación para PC. 

### Costos

Listar los componentes utilizados y su precio de compra.
Incluir la cantidad de horas utilizadas, discriminando por armado del hardware y desarrollo del firmware.

### Resultados y conclusiones

En esta sección se hará un análisis acerca de los resultados obtenidos:

* Incluir los cálculos involucrados en las decisiones de proyecto. Argumentar los valores de componentes, analizar consumos de corriente.
* Comentar no solamente los resultados obtenidos, sino también los obstáculos que se presentaron y cómo se resolvieron, si eso fue posible.


### Entregables

El informe se entrega en formato pdf. Nombre de archivo: TP{n}-{padrón}.pdf. Por ejemplo: TP1-126812.pdf. Incluir *todas* las secciones mencionadas anteriormente (Carátula, Objetivos, Descripción, Hardware, Firmware / Software, Costos, Resultados y conclusiones). Numerar cada página del informe y de ser posible cada renglón, para simplificar el proceso de evaluación.

El proyecto de firmware se entrega comprimido en un zip. Incluir dentro del zip un archivo txt con la aplicación utilizada. Por ejemplo, "Atmel Studio 7.0". Nombre de archivo: TP{n}-{padrón}.zip.
