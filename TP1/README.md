# Laboratorio de Microprocesadores, segundo cuatrimestre 2023

## Trabajo Práctico 1:

Los objetivos de este trabajo práctico son familizarse con un ambiente de desarrollo de microcontroladores y experimentar el uso de puertos (_GPIO: "General Purpose Input Output"_).

## Actividades

### Ambiente de desarrollo y generalidades del microcontrolador
1. Instalar el ambiente de desarrollo, según la computadora que se use. Hay documentación en el Campus, para Windows y Linux.
2. Investigar y discutir cuáles son los métodos posibles para grabarle un programa o _firmware_ al microcontrolador ATmega328P y cuáles son las fuentes posibles para su oscilador.


Para desarrollar _firmware_, se propone primero plantear un diagrama de flujo que responda al requerimiento, luego implementar la codificación, generar el ejecutable, grabar el ejecutable en el microcontrolador y verificar su funcionamiento. Este proceso a menudo es iterativo.

### GPIO BÁSICO
3. Desarrollar un _firmware_ que encienda la mitad de los _LEDs_ disponibles y apague la otra mitad. Ensamblarlo, grabarlo en el microcontrolador y verificar su funcionamiento.
4. Partiendo del _firmware_ anterior, desarrollar otro programa que lea un pulsador. Cada vez que se presiona el pulsador, los _LEDs_ que estaban encendidos deben apagarse y los que estaban apagados deben encenderse. Considerar que una entrada no puede quedar en un estado indeterminado ("al aire").
Pregunta: ¿siempre se observa la transición de los _LEDs_ cuando se aprieta el pulsador? En caso de identificar algún inconveniente, discutir qué puede estar ocurriendo y cómo podría resolverse.

### _PULLUPS_
5. Partiendo del _firmware_ anterior, desarrollar otro en el cual se reemplace el resistor de _pullup_ junto al pulsador por uno interno al microcontrolador.

### ANTI REBOTE (_DEBOUNCE_)
6. Partiendo del _firmware_ anterior, incorporarle un filtrado que evite los rebotes.

### GPIO POR INTERRUPCIONES Y TABLA DE CONSTANTES
7. Implementar un _firmware_ para _display_ de siete segmentos y dos pulsadores. Un pulsador estará conectado a la entrada INT0 y el otro a una entrada que pueda detectar interrupciones por cambios de estado ("_pin change_"). Configurar INT0 por flanco descendente y la interrupción por _pin change_ para que capture sólo el flanco descendente.  Cada vez que se presione el primer pulsador, el display debe incrementar el dígito mostrado en el display y al presionar el otro pulsador el dígito debe decrementarse.

### C
8. Portar el firmware anterior a lenguaje C.

### IA
9. Usando ChatGPT, obtener la implementación en C del programa anterior y comparar el código fuente generado por inteligencia artificial con el implementado con inteligencia natural.

## Materiales

* 1 display siete segmentos
* 10 resistores de 330 Ohms
* 10 resistores de 10 KOhms
* 2 pulsadores
* 1 microcontrolador ATmega328p
* Proboboard
* Cables Dupont macho macho
* Programador AVR (Club de Robótica/Arduino/etcétera).
