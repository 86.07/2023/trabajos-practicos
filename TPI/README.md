# Laboratorio de Microprocesadores, segundo cuatrimestre 2023

## Trabajo Práctico Integrador: seguidor de línea


![Seguidor de línea](./linefollower.png "Seguidor de línea")

El objetivo de este trabajo práctico es integrar los conceptos de la cursada en el clásico vehículo seguidor de línea.


## Actividades

El seguidor de línea debe cumplir con las siguientes premisas:


* En el inicio del programa, el vehículo debe esperar cinco segundos antes de iniciar cualquier movimiento. 
* Antes de iniciar la marcha, el algoritmo deberá detectar si la pista es blanca con línea negra, o a la inversa.
* Los sensores que detectan la línea deben leerse continuamente. La decisión de corregir el rumbo debe tomarse de acuerdo a un filtrado independiente para cada sensor. Por ejemplo se puede usar mediana móvil/"majority voting" de 5 puntos. Experimentar diferentes tamaños de ventana.
* En caso de no detectar línea durante dos segundos, el móvil debe detenerse.
* Para el desarrollo, la alimentación eléctrica puede ser externa, para no consumir pilas. Se propone fuente de alimentación de 9V, 1A mínimo. 
* La velocidad de cada rueda debe regularse mediante sus respectivos controles PWM. En tramos rectos, se sugiere experimentar incrementos de la velocidad. Una curva a 90 grados se anticipa con dos líneas horizontales. Posiblemente se organice una competencia!

![Rally](./rally.png "Rally")

## Materiales

* sensores infrarrojos, mínimo 2
* 2 moto reductores de 6 a 9 V
* 2 controladores de motor DC
* microcontrolador atmega328p
* programador de microcontroladores avr (Club de Robótica/Arduino/avrasp/etcétera).
* fuente de alimentación 9V 1A
* placa experimental
* 2 ruedas, estructura para vehículo